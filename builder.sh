source $stdenv/setup

mkdir /tmp/test_build4

cd /tmp/test_build4

unpackPhase

cd /tmp/test_build4/source

patchPhase

cp -r ./ $out

rm -rf /tmp/test_build4
