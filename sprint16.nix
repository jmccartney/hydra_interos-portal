let pkgs = import <nixpkgs> {};
    src = pkgs.fetchgitPrivate {
        url = "ssh://git@gitlab.com/obsidian-interos/interos-portal.git";
        # owner = "obsidian-systems";
        # repo = "interos-portal";
        rev = "c3c75ebdc6d57282ae97ce007373ccc83df0cece";
        sha256 = "0d7c52disvggsm7gmsy9n8wmrzqz8130a74phv06xf68k88ddvgl";
    };
    obelisk_json = builtins.fromJSON (builtins.readFile "${src}/.obelisk/impl/github.json");
    obelisk_src = pkgs.fetchFromGitHub { inherit (obelisk_json) owner repo rev sha256; };
in
  pkgs.callPackage "${obelisk_src}/default.nix" {}
