{ callPackage,
  stdenv,
  fetchFromGitHub
}:
let
    release16 =
        { hash = "c3c75ebdc6d57282ae97ce007373ccc83df0cece";
          version = "16";
          meta = "Release 16 + tier limit hot fix.  Current r16 release candidate.";
        };

    release15_rejected =
        { hash = "3e1b3278924aafa93c6cf0fd3afe42b8a15c1dac";
          version = "15";
          meta = "Release 15 + hotfixes (same as below) + hotfix to bump tier limit from 3 to 4.
REJECTED as hotfix candidate – too many issues in tier 4.";
        };

    release15 = 
        { hash = "2ed5447f16b7473407438c74594829dfed7523f5";
          version = "15";
          meta = "Release 15 + hotfixes that we are attempting to deploy to production.";
        };

    release14 =
        { hash = "98dab3efcd2e18f468503836184b2d94678a6a62";
          version = "14";
          meta = "This the version of release 14 with numerous hot fixes that was deployed to production on the 19th.";
        };

    release15_obsidian =
        { hash = "a9e9f7f687df0cd0587a176d0035b2ed2a93e1b4";
          version = "15";
          meta = "Release 15 drop from Obsidian.";
        };
        
    releaseList =
        [ release16
          release15_rejected
          release15
          release14
          release15_obsidian
        ];

    # releaseObelisk = release_src:
    #     let obelisk_json = builtins.fromJSON (builtins.readFile "${release_src}/.obelisk/impl/github.json");
    #         obelisk_src = fetchFromGitHub { inherit (obelisk_json) owner repo rev sha256; };
    #     in  callPackage "${obelisk_src}/default.nix" {};
        
    obelisk = fetchFromGitHub
        { owner = "obsidiansystems";
          repo = "obelisk";
          rev = "e5630e7949cf6c3ff8edad511e8429a47e28b008";
          sha256 = "0a6ipbapl0h5clhhqy9gmdhdl9qk9kjzi8rh8sc4rrsqq94ryw8h";
        };

    baseUrl = "git@github.com:interosinc/app";

    addSource = release:
        release // { src = builtins.fetchGit { rev = release.hash; url = baseUrl; ref = "develop"; }; };

    # addObelisk = release:
    #     release // { obelisk = releaseObelisk release.src; };

    addPackage = release_src:
        release_src // { package = import "${release_src}/default.nix" {}; };

    releasePackages = map (release: addPackage (addSource release)) releaseList;

    releaseFullBuilds = map (release: addNewGlobePatch release.package.exe) releasePackages;

    addNewGlobePatch = exe:
        exe.overrideAttrs ( oldAttrs: { patches = [ ./new-interos-globe.patch ]; } );

    patchRelease = release_src:
        stdenv.mkDerivation {
            name = "platform-release-patch";
            meta = "The patched version of the release to fix the new-globe issue.";
            src = release_src;
            patches = [ ./new-interos-globe.patch ];
            dontBuild = true;
            builder = ./builder.sh;
        };

    patchedReleases = map (x: addPackage (patchRelease x.src)) releasePackages;
in
    stdenv.mkDerivation {
        name = "platform-releases";
        meta = "The full builds of all releases of Interos";
        buildInputs = map (x: x.package.exe) patchedReleases;
        dontBuild = true;
    }
    # map addObelisk releasePackages
